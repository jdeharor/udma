
module r_engine(
    output transaction_complete,
    AXI4_R.master r_chan,
    FIFO_WRITE.master data_fifo
);

    assign transaction_complete = r_chan.rvalid && !data_fifo.full && r_chan.rlast;
    assign r_chan.rready = !data_fifo.full;
    assign data_fifo.write = r_chan.rvalid && !data_fifo.full;
    assign data_fifo.data = r_chan.rdata;

endmodule
